﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;

namespace Algemeen
{
    public class Parameter
    {
        /// <summary>
        /// Haalt de softbrick root folder uit de register keys wanneer mogelijk
        /// Indien niet mogelijk returnt de bovenliggende map van de applicatie
        /// </summary>
        public static string SSbRoot
        {
            get
            {
                try
                {
                    RegistryKey masterKey = Registry.LocalMachine.OpenSubKey("SOFTWARE").OpenSubKey("OrgaTime");
                    if (masterKey != null)
                    {
                        return masterKey.GetValue("tcbase").ToString() + "\\";
                    }
                    else
                    {
                        return (Environment.CurrentDirectory + "\\..\\");
                    }
                }
                catch
                {
                    return (Environment.CurrentDirectory + "\\..\\");
                }
            }

        }

        /// <summary>
        /// Returnt het mandant
        /// </summary>
        public static string SMandant
        {
            get
            {
                return "";
            }
        }
    }
}
