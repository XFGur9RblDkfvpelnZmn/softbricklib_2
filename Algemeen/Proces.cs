﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace Algemeen
{
    public class Proces
    {
        public static List<FileInfo> lfiWrkFiles = new List<FileInfo>();
        public Proces(string arg)
        {
            try
            {
                TextWriter tw = new StreamWriter(Parameter.SSbRoot + "log\\tmp.bat");

                tw.WriteLine("SET TC_MANDANT=" + Parameter.SMandant);
                tw.WriteLine(arg);

                tw.Close();

                //definieer tmp.bat
                ProcessStartInfo p = new System.Diagnostics.ProcessStartInfo(Parameter.SSbRoot + "log\\tmp.bat");

                Process proces = new System.Diagnostics.Process();
                proces.StartInfo = p;
                proces.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                proces.StartInfo.CreateNoWindow = true;

                //start bat file
                proces.Start();
                proces.WaitForExit();
                Log.Verwerking(arg);
            }
            catch (Exception ex)
            {
                Log.LogException(string.Empty, ex);
            }
        }
        public Proces(List<string> args)
        {
            try
            {
                TextWriter tw = new StreamWriter(Parameter.SSbRoot + "log\\tmp.bat");
                tw.WriteLine("SET TC_MANDANT=" + Parameter.SMandant);
                foreach (string arg in args)
                {
                    tw.WriteLine(arg);
                }
                tw.Close();

                //definieer tmp.bat
                ProcessStartInfo p = new System.Diagnostics.ProcessStartInfo(Parameter.SSbRoot + "log\\tmp.bat");
                Process proces = new System.Diagnostics.Process();
                proces.StartInfo = p;
                proces.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                proces.StartInfo.CreateNoWindow = true;
                //start bat file
                proces.Start();
                proces.WaitForExit();
                Log.Verwerking(args);
            }
            catch (Exception ex)
            {
                Log.LogException(string.Empty, ex);
            }
        }
        public static void EventLog(string sCode, string sText)
        {
            new Proces(Parameter.SSbRoot + "bin\\clnt_exp event_log " + sCode + " \"" + sText + "\"");
        }
    }
}
