﻿using System;
using System.Collections.Generic;
using System.Text;
using Algemeen;
using System.IO;
namespace BCC.BiFeitenTabel
{
    class Program
    {
        static void Main(string[] args)
        {
            FileInfo fi = Proces.FiSqlsel("badnum from personeel where sdatp <= 'lsysdat' and edatp >= 'lsysdat'");

            FileInfo fiJrovz5pr = Proces.FiJrovz5pr(DateTime.Now.AddDays(-60), DateTime.Now, NUM.NN, SEQ.BADNUM, 12, MODE.PDETOT2, fi, COMPRESSED.YES, TIMEFRAME.DAY_BY_DAY, 0);
            FileInfo fiCsvRap = Proces.FiCsvrap(fiJrovz5pr, TIMEFORMAT.HOURS_DECIMAL);
            
            List<List<String>> lls = Proces.LlsCsvToList(fiCsvRap);

            //List<List<string>> lls = Proces.LlsJrovz5pr(DateTime.Now.AddDays(-60), DateTime.Now, NUM.NN, SEQ.BADNUM, 12, MODE.PDETOT2, fi, COMPRESSED.YES, TIMEFRAME.DAY_BY_DAY, 0, TIMEFORMAT.HOURS_DECIMAL);
            StreamWriter sw = new StreamWriter(args[0]);
            for (int i= 0; i< lls.Count;i++)
            {
                string sLijn = string.Empty;
                if(i!=0 && lls[i].Count>=2)
                    lls[i][1] = "20"+lls[i][1].Substring(6,2)+lls[i][1].Substring(3,2)+lls[i][1].Substring(0,2);
                foreach (string s in lls[i])
                {
                    sLijn += s + ";";
                }
                if (sLijn.Split(';').Length < 4)
                {
                }
                sw.WriteLine(sLijn);
            }
            sw.Close();
            Proces.VerwijderWrkFiles();
        }
    }
}
