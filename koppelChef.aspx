﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="koppelChef.aspx.cs" Inherits="WebApplication8.koppelChef" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
            DataKeyNames="id" DataSourceID="SqlDataSource1" ForeColor="#333333" 
            GridLines="None">
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <RowStyle BackColor="#EFF3FB" />
            <Columns>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" 
                    ReadOnly="True" SortExpression="id" />
                <asp:BoundField DataField="werknemerSalnum" HeaderText="werknemerSalnum" 
                    SortExpression="werknemerSalnum" />
                <asp:BoundField DataField="afdKp" HeaderText="afdKp" SortExpression="afdKp" />
                
            </Columns>
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
            
        </asp:GridView>
        <asp:Button runat="server" ID="Insert" Text="Insert" CommandName="InsertNew" 
            onclick="Insert_Click" />
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:dbConnectionString %>" 
            DeleteCommand="DELETE from chef WHERE id = @id" 
            InsertCommand="INSERT into chef () values ()" 
            SelectCommand="SELECT id, werknemerSalnum, afdKp FROM chef" 
            UpdateCommand="UPDATE chef SET werknemerSalnum = @werknemerSalnum, afdKp = @afdKp WHERE id = @id">
            <DeleteParameters>
                <asp:Parameter Name="id" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="werknemerSalnum" />
                <asp:Parameter Name="afdKp" />
                <asp:Parameter Name="id" />
            </UpdateParameters>
        </asp:SqlDataSource>
    
    </div>
    </form>
</body>
</html>
