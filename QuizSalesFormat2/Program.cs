﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace QuizSalesFormat2
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                string sPath = args[0];
                try
                {
                    
                    StreamReader sr = new StreamReader(args[0]);
                    string sContent = sr.ReadToEnd();
                    sr.Close();
                    string sContentNew = string.Empty;
                    decimal dUnitPrice = 0m;
                    decimal dDiskValue = 0m;
                    int i = 0;
                    foreach (string sLine in Regex.Split(sContent, "\n"))
                    {

                        string sLineNew = sLine;

                        if (sLine.Length > 0)
                        {
                            
                            try
                            {
                                
                                dUnitPrice = Convert.ToDecimal(sLine.Split(',')[16]);
                                dDiskValue = Convert.ToDecimal(sLine.Split(',')[17]);
                            }
                            catch (Exception ex)
                            {
                                if (i != 0)
                                {
                                    Console.WriteLine("Wrong decimal format");
                                    dUnitPrice = 0m;
                                    dDiskValue = 0m;
                                }
                                else
                                {
                                    sLineNew += ",GrossPrice";
                                }
                            }
                            sLineNew += ","+ (dUnitPrice + dDiskValue).ToString("0.00");
                            sContentNew += sLineNew + Environment.NewLine;

                        }
                    }
                    StreamWriter sw = new StreamWriter(sPath);
                    sw.Write(sContentNew);
                    sw.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

            }
            else
            {
                Console.WriteLine("Parameter 1 (path\\file) cannot be null");
            }

        }
    }
}
