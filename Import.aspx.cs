﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.IO;
using System.Data.Sql;
using System.Data.SqlClient;


namespace WebApplication8
{
    public partial class Import : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Parms.leesParms();
            string path = Parms.expFolder;
            string[] lines;
            SqlConnection conn;
            SqlCommand cmd;
            string lijn;
            int a;
            
            if (Convert.ToString(Request["type"]) == "werknemer")
            {
                #region WERKNEMER
                lines = System.IO.File.ReadAllLines(path + "exp_pers.CSV");
                conn = createConnection();

                cmd = new SqlCommand("DELETE FROM werknemer WHERE LEN(mail) IS NULL AND LEN(type) IS NULL", conn);
                cmd.ExecuteScalar();
                


                foreach (string line in lines)
                {
                    lijn = line.Replace('\'', '"');
                    string[] part = lijn.Split(';');

                    if (Convert.ToInt32(part[6]) >= Convert.ToInt32(DateTime.Now.Year + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00")))
                    {
                        try
                        {
                            SqlCommand insert = new SqlCommand("INSERT into werknemer (badgenr, naam,kp,afd,recordnr,salnum,populatie,voornaam) values ('" + @part[0].Trim() + "','" + @part[1] + "','" + @part[2] + "','" + @part[3] + "','" + @part[4] + "','" + @part[5].Trim() + "','" + @part[7].Trim() + "','" + @part[8].Trim() + "')", conn);
                            insert.ExecuteScalar();
                            Response.Write(part[1] + " is succesvol geimporteerd...<br />");
                        }
                        catch (Exception ex)
                        {
                            try
                            {
                                Response.Write(part[1] + "<b> bestaat met extra gegevens, update uitvoeren:</b>");
                                SqlCommand insert = new SqlCommand("UPDATE werknemer SET badgenr='" + @part[0].Trim() + "', naam='" + @part[1] + "',kp='" + @part[2] + "',afd='" + @part[3] + "',populatie='" + @part[7].Trim() + "' WHERE salnum='" + @part[5].Trim() + "'", conn);
                                insert.ExecuteScalar();
                                Response.Write(part[1] + " is succesvol aangepast...<br />");
                            }
                            catch (Exception ex2)
                            {
                                Response.Write(part[1] + "<b> kon niet geïmporteerd worden. <br />FOUT:</b>" + ex+"<br />"+ex2);
                            }
                        }
                    }
                }
                closeConnection(conn);
                #endregion
            }
            else if (Convert.ToString(Request["type"]) == "planning")
            {
                //////// veranderen
                #region PLANNING
                #region PLANNING_OLD
                //lines = System.IO.File.ReadAllLines(path+"exp_planning.CSV");
                //conn = createConnection();

                //cmd = new SqlCommand("DELETE FROM planning", conn);
                //cmd.ExecuteScalar();



                //foreach (string line in lines)
                //{
                //    lijn = line.Replace('\'', '"');
                //    string[] part = lijn.Split(';');


                //    try
                //    {
                //        SqlCommand insert = new SqlCommand("INSERT into planning (dag, minuten,werknemerRecnum) values ('" + @part[0] + "','" + @part[1] + "','" + @part[2] + "')", conn);
                //        insert.ExecuteScalar();
                //        Response.Write(part[2] + " is succesvol geimporteerd...<br />");
                //    }
                //    catch (Exception ex)
                //    {
                //        Response.Write(part[2] + "<b> kon niet geïmporteerd worden. <br />FOUT:</b>" + ex);
                //    }

                //}
                //closeConnection(conn);
                #endregion
                #region PLANNING_NEW

                lines = System.IO.File.ReadAllLines(path + "exp_dagschema_cena.CSV");
                conn = createConnection();
                DateTime nu = DateTime.Now;
                cmd = new SqlCommand("DELETE FROM planning WHERE dag>='"+nu.ToString("yyyy")+"0101'", conn);
                cmd.ExecuteScalar();
                cmd = new SqlCommand("DELETE FROM planning WHERE dag<'" + nu.AddYears(-1).ToString("yyyyMMdd") + "'", conn);
                cmd.ExecuteScalar();
                a = 0;
                foreach (string line in lines)
                {
                    lijn = line.Replace('\'', '"');
                    string[] part = lijn.Split(';');
                    if (part[3].ToLower().Contains("niet kontraktdag") || part[3].ToLower().Contains("roulement") )
                    {
                        try
                        {
                            SqlCommand insert = new SqlCommand("INSERT into planning (dag, minuten,werknemerSalnum,van) values ('" + @part[1] + "','1','" + @part[0].Trim() + "','0')", conn);
                            insert.ExecuteScalar();
                            Response.Write(part[2] + " is succesvol geimporteerd...<br />");
                        }
                        catch (Exception ex)
                        {
                            Response.Write(part[2] + "<b> kon niet geïmporteerd worden. <br />FOUT:</b>" + ex);
                        }
                    }
                    else if ((a > 0) && (part[2].Trim() != "zz") && (part[2].Trim() != "**"))
                    {
                        try
                        {
                            int tmp = Convert.ToInt32(@part[5].Trim()) - Convert.ToInt32(@part[4].Trim());
                            SqlCommand pop = new SqlCommand("SELECT populatie from werknemer where salnum = '"+ @part[0].Trim()+"'", conn);
                            SqlDataReader rdr = cmd.ExecuteReader();
                            rdr.Read();
                            string populatie = string.Empty;
                            try
                            {
                                populatie = rdr["populatie"].ToString();
                            }
                            catch (Exception ex)
                            {
                            }
                            finally
                            {
                                rdr.Close();
                            }

                            if (tmp >= 480 && populatie == "A")
                            {
                                tmp = tmp - 30;
                            }

                            SqlCommand insert = new SqlCommand("INSERT into planning (dag, minuten,werknemerSalnum,van) values ('" + @part[1] + "','" + tmp.ToString() + "','" + @part[0].Trim() + "','" + @part[4] + "')", conn);
                            insert.ExecuteScalar();
                            Response.Write(part[2] + " is succesvol geimporteerd...<br />");
                        }
                        catch (Exception ex)
                        {
                            Response.Write(part[2] + "<b> kon niet geïmporteerd worden. <br />FOUT:</b>" + ex);
                        }
                    }
                    a++;
                }
                closeConnection(conn);
                #endregion
                #endregion
                ////////
            }
            else if (Convert.ToString(Request["type"]) == "teller")
            {
                #region TELLERS
                lines = System.IO.File.ReadAllLines(path + "exp_saldi.CSV");
                conn = createConnection();

                cmd = new SqlCommand("TRUNCATE TABLE teller", conn);
                cmd.ExecuteScalar();


                a = 0;
                foreach (string line in lines)
                {
                    if (a > 0)
                    {
                        lijn = line.Replace('\'', '"');
                        string[] part = lijn.Split(';');
                        string ucatImp = "";
                        decimal totaal = 0;
                        for (int i = 0; i < 9; i++)
                        {
                            switch (i)
                            {
                                case 0:
                                    ucatImp = "47";
                                    break;
                                case 1:
                                    ucatImp = "21";
                                    break;
                                case 2:
                                    ucatImp = "26";
                                    break;
                                case 3:
                                    ucatImp = "34";
                                    break;
                                case 4:
                                    ucatImp = "49";
                                    break;
                                case 5:
                                    ucatImp = "58";
                                    break;
                                case 6:
                                    ucatImp = "60";
                                    break;
                                case 7:
                                    ucatImp = "62";
                                    break;
                                case 8:
                                    ucatImp = "43";
                                    break;
                            }
                            try
                            {
                                decimal salnum = Convert.ToDecimal(@part[0]);
                                Response.Write(@part[i] + "test");
                                if (part[i+2].Length > 0)
                                {
                                    part[i+2] = part[i+2].Trim();
                                    //part[2] = part[2].Replace('.', ',');
                                    //double saldo = Convert.ToDecimal(@part[2]);
                                    //part[2] = Convert.ToString(saldo);
                                }
                                else
                                {
                                    part[i+2] = "0";
                                }
                                DateTime now = DateTime.Now;
                                if ((ucatImp == "21") || (ucatImp == "34") || (ucatImp == "43") || (ucatImp == "26") || (ucatImp == "47") || (ucatImp == "11"))
                                {
                                    totaal += Convert.ToDecimal(part[i + 2].Replace('.', ','));
                                }
                                SqlCommand insert = new SqlCommand("INSERT into teller (ucatId, werknemerSalnum, saldo,jaar) values ('" + ucatImp + "','" + @part[0].Trim() + "','" + @part[i+2] + "','" + now.Year.ToString() + "')", conn);
                                //Response.Write("INSERT into teller (id, werknemerSalnum, saldo) values ('" + ucatImp + "','" + @part[0] + "','" + @part[i+2] + "')");
                                insert.ExecuteScalar();
                                Response.Write("Het saldo" +@part[i+2]+"  van ucat " + ucatImp + " van werknemer " + part[1] + " is succesvol geimporteerd...<br />");
                                part[i+2] = "0";
                            }
                            catch (Exception ex)
                            {
                                Response.Write("Het saldo van ucat " + ucatImp + " van werknemer " + part[1] + "<b> kon niet geïmporteerd worden. <br />FOUT:</b>" + ex);
                            }
                        }
                        DateTime now2 = DateTime.Now;
                        SqlCommand insert2 = new SqlCommand("INSERT into teller (ucatId, werknemerSalnum, saldo,jaar) values ('100','" + @part[0].Trim() + "','" + totaal.ToString().Replace(',','.') + "','" + now2.Year.ToString() + "')", conn);
                        //Response.Write("INSERT into teller (id, werknemerSalnum, saldo) values ('" + ucatImp + "','" + @part[0] + "','" + @part[i+2] + "')");
                        insert2.ExecuteScalar();
                    }
                    a++;

                }
                closeConnection(conn);
                #endregion
                #region TELLERS_VOLGEND JAAR
                lines = System.IO.File.ReadAllLines(path + "exp_saldi_next_year.CSV");
                conn = createConnection();

                a = 0;
                foreach (string line in lines)
                {
                    if (a > 0)
                    {
                        lijn = line.Replace('\'', '"');
                        string[] part = lijn.Split(';');
                        string ucatImp = "";
                        decimal totaal = 0;
                        for (int i = 0; i < 9; i++)
                        {
                            switch (i)
                            {
                                case 0:
                                    ucatImp = "47";
                                    break;
                                case 1:
                                    ucatImp = "21";
                                    break;
                                case 2:
                                    ucatImp = "26";
                                    break;
                                case 3:
                                    ucatImp = "34";
                                    break;
                                case 4:
                                    ucatImp = "49";
                                    break;
                                case 5:
                                    ucatImp = "58";
                                    break;
                                case 6:
                                    ucatImp = "60";
                                    break;
                                case 7:
                                    ucatImp = "62";
                                    break;
                                case 8:
                                    ucatImp = "43";
                                    break;
                            }
                            try
                            {
                                decimal salnum = Convert.ToDecimal(@part[0]);
                                if (part[i+2].Length > 0)
                                {
                                    part[i+2] = part[i+2].Trim();
                                    //part[2] = part[2].Replace('.', ',');
                                    //double saldo = Convert.ToDecimal(@part[2]);
                                    //part[2] = Convert.ToString(saldo);
                                }
                                else
                                {
                                    part[i+2] = "0";
                                }
                                DateTime now = DateTime.Now;
                                int jaar = now.Year + 1;
                                totaal += Convert.ToDecimal(part[i + 2].Replace('.', ','));
                                SqlCommand insert = new SqlCommand("INSERT into teller (ucatId, werknemerSalnum, saldo,jaar) values ('" + ucatImp + "','" + @part[0].Trim() + "','" + @part[i+2] + "','" + Convert.ToString(jaar) + "')", conn);
                                //Response.Write("INSERT into teller (id, werknemerSalnum, saldo) values ('" + ucatImp + "','" + @part[0] + "','" + @part[i+2] + "')");
                                insert.ExecuteScalar();
                                Response.Write("Het saldo van ucat " + ucatImp + " van werknemer " + part[1] + " is succesvol geimporteerd...<br />");
                                part[i+2] = "0";
                            }
                            catch (Exception ex)
                            {
                                Response.Write("Het saldo van ucat " + ucatImp + " van werknemer " + part[1] + "<b> kon niet geïmporteerd worden. <br />FOUT:</b>" + ex);
                            }
                        }
                        DateTime now2 = DateTime.Now;
                        int jaar2 = now2.Year + 1;
                        SqlCommand insert2 = new SqlCommand("INSERT into teller (ucatId, werknemerSalnum, saldo,jaar) values ('100','" + @part[0].Trim() + "','" + totaal.ToString().Replace(',', '.') + "','" + jaar2.ToString("0000") + "')", conn);
                        //Response.Write("INSERT into teller (id, werknemerSalnum, saldo) values ('" + ucatImp + "','" + @part[0] + "','" + @part[i+2] + "')");
                        insert2.ExecuteScalar();
                    }
                    a++;

                }
                closeConnection(conn);
                #endregion
            }
            else if (Convert.ToString(Request["type"]) == "feestdag")
            {
                #region FEESTDAGEN
                lines = System.IO.File.ReadAllLines(path + "exp_feestdag.CSV");
                conn = createConnection();

                cmd = new SqlCommand("TRUNCATE TABLE feestdag", conn);
                cmd.ExecuteScalar();



                foreach (string line in lines)
                {
                    lijn = line.Replace('\'', '"');
                    string[] part = lijn.Split(';');


                    try
                    {
                        string datum;
                        if (part[0].Trim().Length == 6)
                        {
                            datum = "20" + part[0].Trim();
                        }
                        else
                        {
                            datum = part[0].Trim();
                        }
                        SqlCommand insert = new SqlCommand("INSERT into feestdag (datum, naam) values ('"+ datum + "','" + @part[1] + "')", conn);
                        insert.ExecuteScalar();
                        Response.Write(part[1] + " is succesvol geimporteerd...<br />");
                    }
                    catch (Exception ex)
                    {
                        Response.Write(part[1] + "<b> kon niet geïmporteerd worden. <br />FOUT:</b>" + ex);
                    }

                }
                closeConnection(conn);
                #endregion
            }
            ////////GK WEGHALEN?
            #region AANVRAGEN
            //lines = System.IO.File.ReadAllLines(path+"exp_aanvr.CSV");
            //conn = createConnection();
            //a = 0;
            //foreach (string line in lines)
            //{
            //    if (a > 0)
            //    {
            //        int gk=0;
            //        lijn = line.Replace('\'', '"');
            //        string[] part = lijn.Split(';');

            //        if(Convert.ToInt32(part[2])==52)
            //        {
            //            gk=2;
            //        }
            //        else if(Convert.ToInt32(part[2])==53)
            //        {
            //            gk=3;
            //        }
            //        else if(Convert.ToInt32(part[2])==54)
            //        {
            //            gk=4;
            //        }

            //        try
            //        {
            //            SqlCommand insert = new SqlCommand("UPDATE aanvraagDetail SET status='" + gk + "' WHERE (SELECT werknemerSalnum FROM aanvraag WHERE aanvraag.id=aanvraagDetail.aanvraagId) = '" + part[0] + "' AND datum='" + part[1] + "'", conn);
            //            //Response.Write("UPDATE aanvraagDetail SET status='" + gk + "' WHERE (SELECT werknemerSalnum FROM aanvraag WHERE aanvraag.id=aanvraagDetail.aanvraagId) = '" + part[0] + "' AND datum='" + dag + "'");
            //            insert.ExecuteScalar();
            //            Response.Write(part[1] + " voor " + part[1] + " is succesvol aangepast...<br />");
            //        }
            //        catch (Exception ex)
            //        {
            //            Response.Write(part[1] + " voor " + part[1] + "<b> kon niet aangepast worden. <br />FOUT:</b>" + ex);
            //        }
            //    }
                
            //}
            //closeConnection(conn);
            #endregion
        }

        protected void closeConnection(SqlConnection conn)
        {
            if (conn != null)
            {
                //SLUIT CONNECTIE INDIEN BESTAAT
                conn.Close();
            }
        }

        //  MAAK SQL CONNECTIE
        protected SqlConnection createConnection()
        {
            //  DEFINIEER CONNECTIONSTRING
            string connstring = @"Data Source=localhost\SQLEXPRESS;Initial Catalog=DB;Integrated Security=SSPI;";
            //  VERBIND
            SqlConnection conn = new SqlConnection(connstring);
            conn.Open();
            //RETURN DE CONNECTIE
            return conn;
        }
        public string encodeHtml(string text)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (char c in text)
            {
                if (c > 0) // special chars
                    sb.Append(String.Format("&#{0};", (int)c));
                else
                    sb.Append(c);
            }
            return sb.ToString();
        }
    }
    
}
