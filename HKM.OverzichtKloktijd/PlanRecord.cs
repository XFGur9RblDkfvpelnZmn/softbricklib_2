﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HKM.OverzichtKloktijd
{
    public class PlanRecord
    {
        public string SSalnum, SBadnum;
        public DateTime DtStartTijd;
        public TimeSpan TsTijd;
        public TimeSpan TsPauze;
        public string SAfdkod;
        public string SWplekkod;
        public static List<PlanRecord> lprLijst = new List<PlanRecord>();

        public PlanRecord(string sBadnum,string sSalnum, DateTime dtStartTijd, TimeSpan tsTijd, TimeSpan tsPauze, string sAfdkod, string sWplekkod)
        {
            this.SBadnum = sBadnum;
            this.SSalnum = sSalnum;
            this.DtStartTijd = dtStartTijd;
            this.TsTijd = tsTijd;
            this.TsPauze = tsPauze;
            this.SAfdkod = sAfdkod;
            this.SWplekkod = sWplekkod;
            lprLijst.Add(this);
        }
    }
}
