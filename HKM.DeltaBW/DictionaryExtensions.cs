﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Algemeen;
using System.Text.RegularExpressions;

namespace HKM.DeltaBW
{
    
    public class DictionaryDifference<TKey, TValue>
    {
        public TKey Key
        {
            get;
            set;
        }

        public TValue OriginalValue
        {
            get;
            set;
        }

        public TValue NewValue
        {
            get;
            set;
        }
    }

    static class DictionaryExtensions
    {
        public static Dictionary<string, string> DlsCsvToDictionary(FileInfo fiCsvFile, List<int> liKeyColumns)
        {
            string sInhoud = Proces.SLeesFile(fiCsvFile);
            Dictionary<string, String> llsReturnList = new Dictionary<string, string>();
            int i = 0;
            foreach (string sLijn in Regex.Split(sInhoud, Environment.NewLine))
            {
                List<string> lsLijn = new List<string>();
                foreach (string sVeld in sLijn.Split(';'))
                {
                    if (i != 0)
                    {
                        lsLijn.Add(sVeld.Trim().Replace("+", "").Replace(".", ","));
                    }
                    else
                    {
                        lsLijn.Add(sVeld.Trim());
                    }
                }
                string sKey = string.Empty;
                foreach (int a in liKeyColumns)
                {
                    if (a <= lsLijn.Count - 1)
                        sKey += lsLijn[a] + ";";
                }
                if (sKey.Replace(";", "").Trim().Length > 0)
                {
                    if (!llsReturnList.ContainsKey(sKey))
                    {
                        llsReturnList.Add(sKey, lsLijn[4]);
                    }
                    
                }
                i++;
            }
            return llsReturnList;
        }

        public static IEnumerable<DictionaryDifference<TKey, TValue>> GetDifferencesFrom<TKey, TValue>(
        this IDictionary<TKey, TValue> original,
        IDictionary<TKey, TValue> latest)
        where TValue : IComparable
        {
            foreach (var originalItem in original)
            {
                if (latest.ContainsKey(originalItem.Key))
                {
                    if (originalItem.Value.CompareTo(latest[originalItem.Key]) != 0)
                    {
                        // The key is in the latest but the value is different.
                        yield return new DictionaryDifference<TKey, TValue>
                        {
                            Key = originalItem.Key,
                            OriginalValue = originalItem.Value,
                            NewValue = latest[originalItem.Key]
                        };
                    }
                }
                else
                {
                    // The key is not in the latest dictionary.
                    yield return new DictionaryDifference<TKey, TValue>
                    {
                        Key = originalItem.Key,
                        OriginalValue = originalItem.Value,
                        NewValue = default(TValue)
                    };
                }
            }

            foreach (var newItem in latest)
            {
                if (!original.ContainsKey(newItem.Key))
                {
                    // The key is not in the original dictionary.
                    yield return new DictionaryDifference<TKey, TValue>
                    {
                        Key = newItem.Key,
                        OriginalValue = default(TValue),
                        NewValue = latest[newItem.Key]
                    };
                }
            }
        }
    }
}
