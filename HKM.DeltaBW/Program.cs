﻿using System;
using System.Collections.Generic;
using System.Text;

using System.IO;

namespace HKM.DeltaBW
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamWriter sw = new StreamWriter(@"C:\Users\pdoo\Documents\Visual Studio 2010\Projects\SoftbrickLib_2\HKM.DeltaBW\bin\Debug\log.txt");
            string sStart = DateTime.Now.ToLongDateString() + "|" + DateTime.Now.ToLongTimeString();
            Console.WriteLine(sStart);
            List<int> li = new List<int>();
            li.Add(0);
            li.Add(1);
            li.Add(2);
            li.Add(3);
            Dictionary<string,string> dls1 = DictionaryExtensions.DlsCsvToDictionary(new FileInfo(@"C:\SoftBrick\vst.HKM.dec\export\bw.csv"), li);
            Dictionary<string,string> dls2 = DictionaryExtensions.DlsCsvToDictionary(new FileInfo(@"C:\SoftBrick\vst.HKM.jan\export\bw.csv"), li);
            string sLog = string.Empty;
            int i = 0;
            foreach (var difference in dls1.GetDifferencesFrom(dls2))
            {
                i++;
                if (difference.NewValue != null && difference.OriginalValue != null)
                {
                    //Console.WriteLine(
                    //    "Key {0} was {1} but is now {2}",
                    //    difference.Key.ToString(),
                    //    difference.OriginalValue.ToString(),
                    //    difference.NewValue.ToString());
                    sLog+="Key "+difference.Key.ToString()+" was "+difference.OriginalValue.ToString()+" but is now "+difference.NewValue.ToString()+Environment.NewLine;
                }
                else if (difference.NewValue != null)
                {
                    //Console.WriteLine(
                    //    "Key {0} didn't exist but is now {1}",
                    //    difference.Key.ToString(),
                    //    difference.NewValue.ToString());
                    sLog += "Key " + difference.Key.ToString() + " didn't exist but is now " + difference.NewValue.ToString() + Environment.NewLine;
                }
                else
                {
                    //Console.WriteLine(
                    //    "Key {0} was {1} but is now deleted",
                    //    difference.Key.ToString(),
                    //    difference.OriginalValue.ToString());
                    sLog += "Key " + difference.Key.ToString() + " was " + difference.OriginalValue.ToString() + " but is now deleted"+Environment.NewLine;
                }

                sw.Write(sLog);
                sLog = string.Empty;
            }
            
           
            
            sw.WriteLine("Gestart: " + sStart);
            sw.WriteLine("Einde: "+DateTime.Now.ToLongDateString() + "|" + DateTime.Now.ToLongTimeString());
            sw.WriteLine("Aantal records december: " + dls1.Count.ToString());
            sw.WriteLine("Aantal records januari: " + dls2.Count.ToString());
            sw.WriteLine("Aantal verschillen: " + i.ToString());
            sw.Close();
        }
    }
}
