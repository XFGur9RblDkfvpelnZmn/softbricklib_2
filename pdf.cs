﻿using System;
using System.Collections;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.IO;
using System.Diagnostics;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace WebApplication8
{
    public class pdf
    {
        static public void createDoc(string naam,string beginDag, string eindDag, int type, string vanu, string totu,string aantUren)
        {
            //Parms.leesParms();
            Document myDocument = new Document(PageSize.A4);

            PdfWriter.GetInstance(myDocument, new FileStream(Parms.hhoplaRoot + HttpContext.Current.Session["salnum"] + ".pdf", FileMode.Create));

            // step 3:  Open the document now using
            myDocument.Open();

            // step 4: Now add some contents to the document
            Paragraph p = new Paragraph();
            Paragraph p2 = new Paragraph();
            Paragraph p3 = new Paragraph();
            Paragraph p4 = new Paragraph();
            Paragraph p5 = new Paragraph();
            Paragraph p6 = new Paragraph();
            Paragraph p7 = new Paragraph();
            Paragraph p8 = new Paragraph();
            Paragraph p9 = new Paragraph();
            Paragraph p10 = new Paragraph();
            Paragraph p11 = new Paragraph();
            Paragraph p12 = new Paragraph();
            Paragraph p13 = new Paragraph();
            Paragraph p14 = new Paragraph();

            Font header = new Font(Font.NORMAL, 16, Font.BOLD | Font.UNDERLINE);
            Font boldUnd = new Font(Font.NORMAL, 14, Font.BOLD | Font.UNDERLINE);
            p.Add(new Chunk("AANVRAAG ONBETAALDE AFWEZIGHEID CODE 46",header));
            
            p2.Add(new Chunk("Code 46:",boldUnd));
            p2.Add(new Chunk("Dit voor overeengekomen afwezigheid -> onbetaald", new Font(Font.NORMAL, 14)));

            p3.Add(new Chunk("(op suggestie van de werkgever)"));

            p4.Add(new Chunk("Ik, "+naam+", ondergetekende wens op volgende tijdstippen onbetaalde afwezigheid te nemen :"));

            if (beginDag == eindDag)
            {
                p5.Add(new Chunk("Datum:" + beginDag));
                p6.Add(new Chunk("Aantal uren: " + aantUren));
            }
            else
            {
                p5.Add(new Chunk("Van " + beginDag + " tot en met " + eindDag));

            }
            DateTime now = DateTime.Now;
            p7.Add(new Chunk("Datum aanvraag: "+now.Day+"/"+now.Month+"/"+now.Year));
            p8.Add(new Chunk("Handtekening werknemer: "));
            p9.Add(new Chunk("ANTWOORD WERKGEVER INGEVOLGE AANVRAAG ONBETAALDE AFWEZIGHEID", boldUnd));
            p10.Add(new Chunk("In gevolge uw aanvraag voor toegestane afwezigheid onbetaald kunnen wij u mededelen dat wij ons akkoord/niet akkoord (*) verklaren met uw aanvraag. Tijdens de periode van onbetaalde toegestane afwezigheid zal geen loon worden uitbetaald."));
            p11.Add(new Chunk("datum : .............................."));
            p12.Add(new Chunk("handtekening afdelingsverant-woordelijke: "));
            p13.Add(new Chunk("(*) schrappen wat niet van toepassing is. NA GOEKEURING/WEIGERING WORDT DIT DOCUMENT ONVERWIJLD OVERGEMAAKT AAN DE PERSONEELSDIENST."));
            p14.Add(new Chunk("J/Pers Dienst/Master/OV/code 46 24.01.2003"));


            myDocument.Add(p);
            myDocument.Add(new Paragraph("  "));
            myDocument.Add(p2);
            myDocument.Add(new Paragraph("  "));
            myDocument.Add(p3);
            myDocument.Add(new Paragraph("  "));
            myDocument.Add(p4);
            myDocument.Add(new Paragraph("  "));
            myDocument.Add(p5);
            myDocument.Add(new Paragraph("  "));
            myDocument.Add(p6);
            myDocument.Add(new Paragraph("  "));
            myDocument.Add(p7);
            myDocument.Add(new Paragraph("  "));
            myDocument.Add(p8);
            myDocument.Add(new Paragraph("  "));
            myDocument.Add(new Paragraph("  "));
            myDocument.Add(new Paragraph("  "));
            myDocument.Add(p9);
            myDocument.Add(p10);
            myDocument.Add(p11);
            myDocument.Add(p12);
            myDocument.Add(new Paragraph("  "));
            myDocument.Add(new Paragraph("  "));
            myDocument.Add(new Paragraph("  "));
            myDocument.Add(p13);
            myDocument.Add(new Paragraph("  "));
            myDocument.Add(p14);
            myDocument.Close();
        }           
          
    }
    public class pdf45
    {
        static public void createDoc(string naam, string beginDag, string eindDag, int type, string vanu, string totu, string aantUren)
        {
            //Parms.leesParms();
            Document myDocument = new Document(PageSize.A4);

            PdfWriter.GetInstance(myDocument, new FileStream(Parms.hhoplaRoot + HttpContext.Current.Session["salnum"] + ".pdf", FileMode.Create));

            // step 3:  Open the document now using
            myDocument.Open();

            // step 4: Now add some contents to the document
            Paragraph p = new Paragraph();
            Paragraph p2 = new Paragraph();
            Paragraph p3 = new Paragraph();
            Paragraph p4 = new Paragraph();
            Paragraph p5 = new Paragraph();
            Paragraph p6 = new Paragraph();
            Paragraph p7 = new Paragraph();
            Paragraph p8 = new Paragraph();
            Paragraph p9 = new Paragraph();
            Paragraph p10 = new Paragraph();
            Paragraph p11 = new Paragraph();
            Paragraph p12 = new Paragraph();
            Paragraph p13 = new Paragraph();
            Paragraph p14 = new Paragraph();

            Font header = new Font(Font.NORMAL, 16, Font.BOLD | Font.UNDERLINE);
            Font boldUnd = new Font(Font.NORMAL, 14, Font.BOLD | Font.UNDERLINE);
            p.Add(new Chunk("AANVRAAG ONBETAALDE AFWEZIGHEID CODE 45", header));

            p2.Add(new Chunk("Code 45:", boldUnd));
            p2.Add(new Chunk("Dit voor toegestane afwezigheid -> onbetaald", new Font(Font.NORMAL, 14)));

            p3.Add(new Chunk("Na uitputting van alle vakantie en recuperatie. (op vraag van het personeelslid)"));
            p3.Add(new Chunk("Wordt in principe enkel toegestaan in zeer uitzonderlijke gevallen! Men kan dit enkel opnemen in volledige dagen."));
            p4.Add(new Chunk("Ik, " + naam + ", ondergetekende wens op volgende tijdstippen onbetaalde afwezigheid te nemen :"));

            if (beginDag == eindDag)
            {
                p5.Add(new Chunk("Datum:" + beginDag));
                p6.Add(new Chunk("Aantal uren: " + aantUren));
            }
            else
            {
                p5.Add(new Chunk("Van " + beginDag + " tot en met " + eindDag));

            }
            DateTime now = DateTime.Now;
            p7.Add(new Chunk("Datum aanvraag: " + now.Day + "/" + now.Month + "/" + now.Year));
            p8.Add(new Chunk("Handtekening werknemer: "));
            p9.Add(new Chunk("ANTWOORD WERKGEVER INGEVOLGE AANVRAAG ONBETAALDE AFWEZIGHEID", boldUnd));
            p10.Add(new Chunk("In gevolge uw aanvraag voor toegestane afwezigheid onbetaald kunnen wij u mededelen dat wij ons akkoord/niet akkoord (*) verklaren met uw aanvraag. Tijdens de periode van onbetaalde toegestane afwezigheid zal geen loon worden uitbetaald."));
            p11.Add(new Chunk("datum : .............................."));
            p12.Add(new Chunk("handtekening afdelingsverant-woordelijke: "));
            p13.Add(new Chunk("(*) schrappen wat niet van toepassing is. NA GOEKEURING/WEIGERING WORDT DIT DOCUMENT ONVERWIJLD OVERGEMAAKT AAN DE PERSONEELSDIENST."));
            p14.Add(new Chunk("J/Pers Dienst/Master/OV/code 45 24.01.2003"));


            myDocument.Add(p);
            myDocument.Add(new Paragraph("  "));
            myDocument.Add(p2);
            myDocument.Add(new Paragraph("  "));
            myDocument.Add(p3);
            myDocument.Add(new Paragraph("  "));
            myDocument.Add(p4);
            myDocument.Add(new Paragraph("  "));
            myDocument.Add(p5);
            myDocument.Add(new Paragraph("  "));
            myDocument.Add(p6);
            myDocument.Add(new Paragraph("  "));
            myDocument.Add(p7);
            myDocument.Add(new Paragraph("  "));
            myDocument.Add(p8);
            myDocument.Add(new Paragraph("  "));
            myDocument.Add(new Paragraph("  "));
            myDocument.Add(new Paragraph("  "));
            myDocument.Add(p9);
            myDocument.Add(p10);
            myDocument.Add(p11);
            myDocument.Add(p12);
            myDocument.Add(new Paragraph("  "));
            myDocument.Add(new Paragraph("  "));
            myDocument.Add(new Paragraph("  "));
            myDocument.Add(p13);
            myDocument.Add(new Paragraph("  "));
            myDocument.Add(p14);
            myDocument.Close();
        }

    }
}
