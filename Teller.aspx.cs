﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace WebApplication8
{
    public partial class Teller : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int jaar = Convert.ToInt32(Request["jaar"]);
            getAanvraagRecord get = new getAanvraagRecord();
            Response.Write(get.teller(jaar));
        }
    }
}
