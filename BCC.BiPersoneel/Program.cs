﻿using System;
using System.Collections.Generic;
using System.Text;
using Algemeen;
using System.IO;

namespace BCC.BiPersoneel
{
    class Program
    {
        static void Main(string[] args)
        {
            string sSqlCmd = "salnum,naam,init,plaats,afdkod,wplekkod,funktie,wochsoll,ma_grp from personeel";
            List<string> lsHeaders = Proces.lsSqlSelHeaders(sSqlCmd);
            Dictionary<string, List<string>> dls = Proces.DlsSqlSel(sSqlCmd);
            StreamWriter sw = new StreamWriter(args[0]);

            for (int i = 0; i < lsHeaders.Count;i++ )
            {
                if (i < lsHeaders.Count - 1)
                    sw.Write(lsHeaders[i] + ";");
                else
                    sw.Write(lsHeaders[i]);
            }
            sw.Write(Environment.NewLine);
            foreach (KeyValuePair<string, List<string>> kvp in dls)
            {
                try
                {
                    kvp.Value[6] = (Convert.ToDecimal(kvp.Value[6])/60).ToString("0.00");
                }
                catch (Exception ex)
                {
                }
                sw.WriteLine(kvp.Key + ";" + kvp.Value[0] + ";" + kvp.Value[1] + ";" + kvp.Value[2] + ";" + kvp.Value[3] + ";" + kvp.Value[4] + ";" + kvp.Value[5] + ";" + kvp.Value[6] + ";" + kvp.Value[7]+";");
            }
            sw.Close();
            Proces.VerwijderWrkFiles();

        }
    }
}

