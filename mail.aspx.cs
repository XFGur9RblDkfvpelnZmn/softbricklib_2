﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.Sql;
using System.Data.SqlClient;

namespace WebApplication8
{
    public partial class mail : System.Web.UI.Page
    {
        
        //  SLUIT CONNECTIES
        protected void closeConnection(SqlConnection conn)
        {
            if (conn != null)
            {
                //SLUIT CONNECTIE INDIEN BESTAAT
                conn.Close();
            }
        }

        //  MAAK SQL CONNECTIE
        protected SqlConnection createConnection()
        {
            //  DEFINIEER CONNECTIONSTRING
            string connstring = @"Data Source=localhost\SQLEXPRESS;Initial Catalog=DB;Integrated Security=SSPI;";
            //  VERBIND
            SqlConnection conn = new SqlConnection(connstring);
            conn.Open();
            //RETURN DE CONNECTIE
            return conn;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection conn = createConnection();
            SqlDataReader rdr = null;
            string email="";
            try
            {
                //  VOER SQL CMD UIT
                SqlCommand cmd = new SqlCommand("SELECT mail FROM werknemer WHERE type='adm'", conn);
                rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    if (Convert.ToString(rdr["mail"]).Length > 4)
                    {
                        email += rdr["mail"] + ";";
                    }
                }
            }
            finally
            {
                // SLUIT READER
                if (rdr != null)
                {
                    rdr.Close();
                }
            }
            try
            {
                //  VOER SQL CMD UIT
                SqlCommand cmd = new SqlCommand(@"SELECT 
	(SELECT extra.type FROM aanvraagDetail AS extra WHERE extra.aanvraagId=aanvraagDetail.extraAanvraag) AS 'extraType',
	(SELECT extra.van FROM aanvraagDetail AS extra WHERE extra.aanvraagId=aanvraagDetail.extraAanvraag) AS 'extraVan',
	(SELECT extra.tot FROM aanvraagDetail AS extra WHERE extra.aanvraagId=aanvraagDetail.extraAanvraag) AS 'extraTot',
	aanvraagDetail.extraAanvraag,aanvraagDetail.van, aanvraagDetail.tot, aanvraagDetail.type, gegeven.id,werknemer.naam,werknemer.afd, werknemer.voornaam,
	gegeven.werknemerSalnum, 
	SUBSTRING(MIN(aanvraagDetail.datum), 7, 2) + '/' + SUBSTRING(MIN(aanvraagDetail.datum), 5, 2) + '/' + SUBSTRING(MIN(aanvraagDetail.datum), 1, 4) AS Begindag, 
	SUBSTRING(MAX(aanvraagDetail.datum), 7, 2) + '/' + SUBSTRING(MAX(aanvraagDetail.datum), 5, 2) + '/' + SUBSTRING(MAX(aanvraagDetail.datum), 1, 4) AS Einddag,
	(SELECT     MAX(mail) 
		FROM werknemer as wn 
		INNER JOIN chef as chef
		ON wn.salnum = chef.werknemerSalnum WHERE (chef.afdKp = werknemer.afd)    ) AS mail 
	
	FROM         aanvraag AS gegeven 
	INNER JOIN aanvraagDetail ON gegeven.id = aanvraagDetail.aanvraagId 
	INNER JOIN werknemer ON gegeven.werknemerSalnum = werknemer.salnum 
	WHERE (aanvraagDetail.export='0' OR aanvraagDetail.export IS NULL) AND   (aanvraagDetail.status < 1) AND aanvraagDetail.datum<>'00000000'
	 GROUP BY gegeven.werknemerSalnum, aanvraagDetail.aanvraagId,werknemer.naam,werknemer.afd,gegeven.id,aanvraagDetail.type,aanvraagDetail.van,aanvraagDetail.tot,aanvraagDetail.extraAanvraag,werknemer.kp, aanvraagDetail.export,werknemer.voornaam", conn);
                rdr = cmd.ExecuteReader();

                //  VOOR ELK RECORD
                while (rdr.Read())
                {
                    Response.Write("|");
                    if ((Convert.ToString(rdr["mail"]).Length > 4)&&(rdr["mail"]!=null))
                    {
                        
                        System.Web.Mail.MailMessage message = new System.Web.Mail.MailMessage();
                        message.From = "bv05@retail-sc.com";
                        message.To = Convert.ToString(rdr["mail"]);
                        message.Subject = "HHopla: Openstaande aanvraag van " + rdr["naam"] + rdr["voornaam"];
                        string mail = "Hallo,<br />Onderstaande medewerker heeft een aanvraag voor afwezigheid ingediend via HHopla.<br />Kan je dit checken in de planning en behandelen via de knoppen in deze mail?<br /><br />Naam: ";
                        mail += Convert.ToString(rdr["naam"]);
                        mail += "<br />";
                        mail += "Afdeling: ";
                        mail += rdr["afd"];
                        mail += "<br />Periode afwezigheid: ";
                        mail += rdr["beginDag"] + "-" + rdr["eindDag"];
                        mail += "<br />Aanvraag type: ";
                        switch (Convert.ToInt32(rdr["type"]))
                        {
                            case 1:
                                mail += "Hele dag";
                                break;
                            case 2:
                                mail += "Voormiddag";
                                break;
                            case 3:
                                mail += "Namiddag";
                                break;
                            case 0:
                                mail += "Van " + Convert.ToString(rdr["van"]) + " tot " + Convert.ToString(rdr["tot"]);
                                break;
                        }
                        if (Convert.ToInt32(rdr["extraAanvraag"]) > 0)
                        {
                            mail+= "<br /><br /> LET OP, ER IS EEN EXTRA AANVRAAG GEMAAKT VOOR DEZE DAG, INDIEN U DEZE AANVRAAG GOEDKEURT WORDT DE PLANNING VOOR DEZE DAG GEWIST!";
                            switch (Convert.ToInt32(rdr["extraType"]))
                            {
                                case 0:
                                    mail += "<br /><br />De extra aanvraag is van "+Convert.ToString(rdr["extraVan"])+" tot "+Convert.ToString(rdr["extraTot"]);
                                    break;
                                case 2:
                                    mail += "<br /><br />De extra aanvraag is een namiddag.";
                                    break;
                                case 3:
                                    mail += "<br /><br />De extra aanvraag is een namiddag.";
                                    break;
                            }
                        }
                        mail += "<br /><br /><table><tr><td bgcolor='green'><a href='http://10.0.216.237/chf.aspx?nmbr=" + rdr["id"] + "&type=4'>Goedkeuren</a><br />";
                        mail += "</td></tr><tr><td bgcolor='lightgreen'><a href='http://10.0.216.237/chf.aspx?nmbr=" + rdr["id"] + "&type=3'>In wacht</a><br />";
                        mail += "</td></tr><tr><td bgcolor='red'><a href='http://10.0.216.237/chf.aspx?nmbr=" + rdr["id"] + "&type=2'>Afkeuren</a></td></tr></table><br /><br />";
                        message.Body = mail;
                        System.Web.Mail.SmtpMail.SmtpServer = "10.0.243.143";
                        message.BodyFormat = System.Web.Mail.MailFormat.Html;
                        //message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", 1);
                        //message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "info@meerisbeter.be");
                        //message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "minderisslechter");
                        System.Web.Mail.SmtpMail.Send(message);
                        Response.Write(message.Body);
                    }
                        else
                    {
                        System.Web.Mail.MailMessage message = new System.Web.Mail.MailMessage();
                        message.From = "bv05@retail-sc.com";
                        message.To = email;
                        message.Subject = "HHopla: Openstaande aanvraag van " + rdr["naam"] + rdr["voornaam"];
                        string mail = "Hallo,<br />Onderstaande medewerker heeft een aanvraag voor afwezigheid ingediend via HHopla.<br />Kan je dit checken in de planning en behandelen via de knoppen in deze mail?<br /><br />Naam: ";
                        mail += Convert.ToString(rdr["naam"]);
                        mail += "<br />";
                        mail += "Afdeling: ";
                        mail += rdr["afd"];
                        mail += "<br />Periode afwezigheid: ";
                        mail += rdr["beginDag"] + "-" + rdr["eindDag"];
                        mail += "<br />Aanvraag type: ";
                        switch (Convert.ToInt32(rdr["type"]))
                        {
                            case 1:
                                mail += "Hele dag";
                                break;
                            case 2:
                                mail += "Voormiddag";
                                break;
                            case 3:
                                mail += "Namiddag";
                                break;
                            case 0:
                                mail += "Van " + Convert.ToString(rdr["van"]) + " tot " + Convert.ToString(rdr["tot"]);
                                break;
                        }
                        if (Convert.ToInt32(rdr["extraAanvraag"]) > 0)
                        {
                            mail += "<br /><br /> LET OP, ER IS EEN EXTRA AANVRAAG GEMAAKT VOOR DEZE DAG, INDIEN U DEZE AANVRAAG GOEDKEURT WORDT DE PLANNING VOOR DEZE DAG GEWIST!";
                            switch (Convert.ToInt32(rdr["extraType"]))
                            {
                                case 0:
                                    mail += "<br /><br />De extra aanvraag is van " + Convert.ToString(rdr["extraVan"]) + " tot " + Convert.ToString(rdr["extraTot"]);
                                    break;
                                case 2:
                                    mail += "<br /><br />De extra aanvraag is een namiddag.";
                                    break;
                                case 3:
                                    mail += "<br /><br />De extra aanvraag is een namiddag.";
                                    break;
                            }
                        }
                        mail += "<br /><br /><table><tr><td bgcolor='green'><a href='http://10.0.216.237/chf.aspx?nmbr=" + rdr["id"] + "&type=4'>Goedkeuren</a><br />";
                        mail += "</td></tr><tr><td bgcolor='lightgreen'><a href='http://10.0.216.237/chf.aspx?nmbr=" + rdr["id"] + "&type=3'>In wacht</a><br />";
                        mail += "</td></tr><tr><td bgcolor='red'><a href='http://10.0.216.237/chf.aspx?nmbr=" + rdr["id"] + "&type=2'>Afkeuren</a></td></tr></table><br /><br />";
                        mail += "<b><br /><br />VOOR DE BETREFFENDE CHEF IS GEEN EMAILADRES INGEVULD!";
                        message.Body = mail;
                        System.Web.Mail.SmtpMail.SmtpServer = "10.0.243.143";
                        //System.Web.Mail.SmtpMail.SmtpServer = "smtp.zakelijkmail.nl";
                        message.BodyFormat = System.Web.Mail.MailFormat.Html;
                        //message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", 1);
                        //message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "info@meerisbeter.be");
                        //message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "minderisslechter");
                        System.Web.Mail.SmtpMail.Send(message);
                        Response.Write(message.Body);
                    }
                }
                rdr.Close();
                SqlCommand cmd2 = new SqlCommand("UPDATE aanvraagDetail  SET aanvraagDetail.export=1 WHERE aanvraagDetail.export<2  AND aanvraagDetail.datum<>'00000000'", conn);
                rdr = cmd2.ExecuteReader();

                
            }
            finally
            {
                // SLUIT READER
                if (rdr != null)
                {
                    rdr.Close();
                }
                //  SLUIT SQL CONNECTIE
                closeConnection(conn);
            }
            

        }
    }
}
