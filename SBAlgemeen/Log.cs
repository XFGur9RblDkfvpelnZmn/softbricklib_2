﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Algemeen
{
    public class Log
    {
        /// <summary>
        /// log verwerking van object
        /// </summary>
        /// <param name="oThis">Object dat verwerkt wordt</param>
        public static void Verwerking(object oThis)
        {
            try
            {
                StreamWriter sw = File.AppendText(Parameter.SSbRoot + "log\\SBLib.log");
                sw.WriteLine();
                sw.WriteLine(DateTime.Now.ToShortDateString() + " - " + DateTime.Now.ToLongTimeString());
                sw.WriteLine("---------------------");
                sw.WriteLine("Verwerking van: " + oThis.ToString());
                sw.WriteLine();
                sw.Close();
            }
            catch (Exception logEx)
            {
                LogException(oThis, logEx);
            }
        }
        /// <summary>
        /// log verwerking van object + bericht
        /// </summary>
        /// <param name="oThis"></param>
        /// <param name="sBericht"></param>
        public static void Verwerking(object oThis, string sBericht)
        {
            try
            {
                StreamWriter sw = File.AppendText(Parameter.SSbRoot + "log\\SBLib.log");
                sw.WriteLine();
                sw.WriteLine(DateTime.Now.ToShortDateString() + " - " + DateTime.Now.ToLongTimeString());
                sw.WriteLine("---------------------");
                sw.WriteLine("Verwerking van: " + oThis.ToString());
                sw.WriteLine(sBericht);
                sw.WriteLine();
                sw.Close();
            }
            catch (Exception logEx)
            {
                LogException(oThis.ToString() + " - " + sBericht, logEx);
            }
        }
        /// <summary>
        /// log verwerking, alleen bericht
        /// </summary>
        /// <param name="sBericht"></param>
        public static void Verwerking(string sBericht)
        {
            try
            {
                StreamWriter sw = File.AppendText(Parameter.SSbRoot + "log\\SBLib.log");
                sw.WriteLine();
                sw.WriteLine(DateTime.Now.ToShortDateString() + " - " + DateTime.Now.ToLongTimeString());
                sw.WriteLine("---------------------");
                sw.WriteLine(sBericht);
                sw.WriteLine();
                sw.Close();
            }
            catch (Exception logEx)
            {
                LogException(sBericht, logEx);
            }
        }
        /// <summary>
        /// log een foutmelding
        /// </summary>
        /// <param name="ex"></param>
        public static void Exception(Exception ex)
        {
            try
            {
                Proces.EventLog("SBLIB", "SB Library Error : Kijk SBLib.log na in de log folder");
                StreamWriter sw = File.AppendText(Parameter.SSbRoot + "log\\SBLib.log");
                sw.WriteLine();
                sw.WriteLine(DateTime.Now.ToShortDateString() + " - " + DateTime.Now.ToLongTimeString());
                sw.WriteLine("---------------------");
                sw.WriteLine(ex.ToString());
                sw.WriteLine();
                sw.Close();
            }
            catch (Exception logEx)
            {
                LogException(ex, logEx);
            }
        }
        /// <summary>
        /// log een foutmelding wanneer de log niet beschikbaar is
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="logEx"></param>
        public static void LogException(object ex, Exception logEx)
        {

            StreamWriter sw = File.AppendText(AppDomain.CurrentDomain.BaseDirectory + "\\SBLib.log");
            sw.WriteLine();
            sw.WriteLine(DateTime.Now.ToShortDateString() + " - " + DateTime.Now.ToLongTimeString());
            sw.WriteLine("---------------------");
            sw.WriteLine("LOG:");
            sw.WriteLine(ex.ToString());
            sw.WriteLine();
            sw.WriteLine("LOG EXCEPTION:");
            sw.WriteLine(logEx.ToString());
            sw.WriteLine();
            sw.Close();
        }
    }
}
