﻿using System;
using System.Collections.Generic;
using System.Text;
using Algemeen;
using System.IO;
using System.Globalization;

namespace VER.SalarisInterface
{
    class Wochsoll
    {
        public static List<Wochsoll> lwWochsoll = new List<Wochsoll>();
        string sSalnum;
        public int wochsoll;
        public int filler;
        int week;
        public Wochsoll(string sSalnum, DateTime dtDatum)
        {
            this.sSalnum = sSalnum;
            List<string> lsArgs = new List<string>();
            lsArgs.Add("-s");
            lsArgs.Add("-d;");
            lsArgs.Add("-c" + dtDatum.ToString("yyMMdd"));
            FileInfo fi = Proces.FiSqlsel("personeel.wochsoll personeel.filler2 from personeel where salnum = '"+sSalnum+"'",lsArgs);
            string sInhoud = Proces.SLeesFile(fi);
            int wochsoll = 0;
            int filler = 0;
            Int32.TryParse(sInhoud.Split(';')[0], out wochsoll);
            int iWochsollMin = Convert.ToInt32(Convert.ToDecimal(wochsoll%60)/60*100);
            int iWochsollUur = wochsoll/60;
            wochsoll = iWochsollUur * 100 + iWochsollMin;
            Int32.TryParse(sInhoud.Split(';')[1], out filler);
            int iFillerMin = Convert.ToInt32(Convert.ToDecimal(filler % 60) / 60 * 100);
            int iFillerUur = filler / 60;
            filler = iFillerUur * 100 + iFillerMin;
            this.wochsoll = wochsoll;
            this.filler = filler;
            //BEREKEN WEEK VH JAAR
            CultureInfo myCI = new CultureInfo("nl-BE");
            Calendar myCal = myCI.Calendar;

            // Gets the DTFI properties required by GetWeekOfYear.
            CalendarWeekRule myCWR = myCI.DateTimeFormat.CalendarWeekRule;
            DayOfWeek myFirstDOW = myCI.DateTimeFormat.FirstDayOfWeek;

            int week = myCal.GetWeekOfYear(dtDatum, myCWR, myFirstDOW);
            DayOfWeek laatsteDag = new DateTime(dtDatum.Year, 12, 31).DayOfWeek;
            if ((week == 1) && (laatsteDag != DayOfWeek.Sunday))
            {
                this.week = myCal.GetWeekOfYear(new DateTime(dtDatum.Year, 12, 31), myCWR, myFirstDOW);
            }
            else
            {
                this.week = week;
            }
            if (week > 53)
            {
                week = 1;
            }
            
            lwWochsoll.Add(this);
        }
        public static Wochsoll wZoekBijlage(string sSalnum, int week)
        {
            // Find a book by its ID.
            Wochsoll result = lwWochsoll.Find(
            delegate(Wochsoll w)
            {
                return w.sSalnum == sSalnum && w.week == week;
            }
            );
            return result;
        }
    }
}
