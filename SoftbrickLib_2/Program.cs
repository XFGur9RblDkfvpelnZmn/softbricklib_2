﻿using System;
using System.Collections.Generic;
using System.Text;
using Algemeen;
using System.IO;
using System.Globalization;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace VER.SalarisInterface
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Algemeen.Parameter.SMandant = "BEL";
            Log.Verwerking("Start Salarisverwerking");
            string sWerkgeverNr = Proces.SLeesFile(Proces.FiSqlsel("clientnum from msyspar")).Trim();
            string sGWTCode = Proces.SLeesFile(Proces.FiSqlsel("compnr50 from msyspar")).Trim();

            int iStartDat = 29999999;
            int iEindDat = 0;
            foreach (string sLijn in Regex.Split(Proces.SLeesFile(new FileInfo(Parameter.SSbRoot + "log\\expblox1.txt")), Environment.NewLine))
            {
                if (sLijn.Length > 28)
                {
                    if (Convert.ToInt32(sLijn.Substring(14, 6)) < iStartDat)
                    {
                        iStartDat = Convert.ToInt32(sLijn.Substring(14, 6));
                    }
                    else if (Convert.ToInt32(sLijn.Substring(14, 6)) > iEindDat)
                    {
                        iEindDat = Convert.ToInt32(sLijn.Substring(14, 6));
                    }

                }
            }

            //DEFINIEER ARGUMENTEN
            DateTime dtBeginDat = Proces.dtStringToDate(iStartDat.ToString());
            DateTime dtEindDat = Proces.dtStringToDate(iEindDat.ToString());


            Dictionary<string, List<string>> dssLoonComp = Proces.DlsSqlSel("catkod, catoms[8] from urencategorie");
            
            string sNewSalinf = string.Empty;
            //lees salarisinterface in objecten
            foreach(string sLijn in Regex.Split(Proces.SLeesFile(new FileInfo(Parameter.SSbRoot+"log\\expblox1.txt")),Environment.NewLine))
            {
                try
                {
                    string sLooncomp = string.Empty;
                    if (dssLoonComp.ContainsKey(sLijn.Substring(22, 2)))
                    {
                        sLooncomp = dssLoonComp[sLijn.Substring(22, 2)][0];
                    }
                    
                    //string sLooncomp = Proces.SLeesFile(Proces.FiSqlsel("catoms[8] from urencategorie where catkod = '" + sLijn.Substring(22, 2) + "'")).Trim();
                    if (sLijn.Substring(20, 4) == sGWTCode)
                    {
                        sLooncomp = sGWTCode;
                    }
                    else if (sLijn.Substring(20, 4) == "0000")
                    {
                        sLooncomp = "0000";
                    }
                    if (sLooncomp.Length == 4)
                    {
                        Record r = new Record(sLijn.Substring(7, 7), sWerkgeverNr, sLijn.Substring(14, 6), sLooncomp, sLijn.Substring(24, 4));
                    }

                }
                catch (Exception ex)
                {
                    if (sLijn.Trim().Length > 0)
                    {
                        Log.Verwerking("regel niet verwerkt: " + sLijn);
                        Log.Exception(ex);
                    }
                }
                finally
                {
                    Proces.VerwijderWrkFiles();
                }
            }

            //verwerk meeruren
            foreach (Record r in Record.lrRecords)
            {
                if ((r.sLooncode == "7018") && (r.dtDatum.DayOfWeek == DayOfWeek.Sunday))
                {
                    Record.lrNewRecords.AddRange(r.omboekMeerUren());
                }

                Wochsoll w = Wochsoll.wZoekBijlage(r.sSalnum, r.week);
                if (w == null)
                {
                    w = new Wochsoll(r.sSalnum, r.dtDatum);
                }
                if (w.wochsoll > w.filler)
                {
                    int iBijlage = w.wochsoll - w.filler;
                    foreach (Record record in Record.lrRecords)
                    {

                        if ((record.sSalnum == r.sSalnum) && (record.sLooncode == "7010") && (iBijlage > 0) && (record.week == r.week) && (record.dtDatum.DayOfWeek != DayOfWeek.Sunday))
                        {
                            if (record.iUren == iBijlage)
                            {
                                record.sLooncode = "7018";
                                iBijlage = 0;
                            }
                            else if (record.iUren < iBijlage)
                            {
                                record.sLooncode = "7018";
                                iBijlage = iBijlage - record.iUren;
                            }
                            else
                            {
                                record.iUren = record.iUren - iBijlage;


                                Record rNieuw = new Record(record.sSalnum, record.sWerkgeverNr, record.sDatum, "7018", iBijlage.ToString("0000"),true);
                                Record.lrNewRecords.Add(rNieuw);
                                iBijlage = 0;
                            }
                        }
                    }
                    w.wochsoll = w.filler;
                }

            }
            Record.lrRecords.AddRange(Record.lrNewRecords);

            foreach (Record r in Record.lrRecords)
            {
                r.checkDubbel(Record.lrRecords);
                if (((r.sLooncode == "7010") || (r.sLooncode == "0000")) && (r.iUren == 0) && (r.bestaatRecordZelfdeDag()))
                {
                    r.sSalnum = "weg";
                }
                
            }
            Record.lrRecords.Sort();
            FileInfo fi = new FileInfo(Parameter.SSbRoot + "log\\expblox_vw.txt");
            TextWriter tw = new StreamWriter(fi.FullName);
            foreach (Record r in Record.lrRecords)
            {
                if (r.sSalnum != "weg")
                {
                    tw.Write(r.sWerkgeverNr+r.sSalnum+"K"+r.dtDatum.ToString("yyyyMMdd")+r.sLooncode+r.iUren.ToString("0000")+Environment.NewLine);
                    
                }
            }
            
            tw.Close();
            Proces.Tc_lp(fi);
            Log.Verwerking("Einde Salarisinterface");
        }
        
    }
}
