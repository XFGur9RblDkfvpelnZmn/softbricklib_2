﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.Collections;

namespace VER.SalarisInterface
{
    class Record : IComparable
    {
        public string sSalnum, sWerkgeverNr, sDatum, sLooncode;
        public int iUren;
        public static List<Record> lrRecords = new List<Record>();
        public static List<Record> lrNewRecords = new List<Record>();
        public int week;
        public DateTime dtDatum;


        public Record(string sSalnum, string sWerkgeverNr, string sDatum, string sLooncode, string sUren)
        {
            while (sSalnum.Length < 7)
                sSalnum = "0" + sSalnum;

            while (sSalnum.Length > 7)
                sSalnum = sSalnum.Substring(1);
            this.sSalnum = sSalnum;
            this.sWerkgeverNr = sWerkgeverNr;
            this.sDatum = sDatum;
            this.sLooncode = sLooncode;

            try
            {
                
                this.iUren =(int)(Convert.ToInt32(sUren));

            }
            catch (Exception ex)
            {
                this.iUren = 0;
                Algemeen.Log.Verwerking("record niet correct: " + sSalnum + " | " + sDatum);
                Algemeen.Log.Exception(ex);
            }
            this.dtDatum = Algemeen.Proces.dtStringToDate(sDatum);
            //BEREKEN WEEK VH JAAR
            CultureInfo myCI = new CultureInfo("nl-BE");
            Calendar myCal = myCI.Calendar;

            // Gets the DTFI properties required by GetWeekOfYear.
            CalendarWeekRule myCWR = myCI.DateTimeFormat.CalendarWeekRule;
            DayOfWeek myFirstDOW = myCI.DateTimeFormat.FirstDayOfWeek;

            int week = myCal.GetWeekOfYear(dtDatum, myCWR, myFirstDOW);
            DayOfWeek laatsteDag = new DateTime(dtDatum.Year, 12, 31).DayOfWeek;
            if ((week == 1) && (laatsteDag != DayOfWeek.Sunday))
            {
                this.week = myCal.GetWeekOfYear(new DateTime(dtDatum.Year, 12, 31), myCWR, myFirstDOW);
            }
            else
            {
                this.week = week;
            }
            if (week > 53)
            {
                week = 1;
            }
            lrRecords.Add(this);
        }
        public Record(string sSalnum, string sWerkgeverNr, string sDatum, string sLooncode, string sUren, bool bGeenLijst)
        {
            while (sSalnum.Length < 7)
                sSalnum = "0" + sSalnum;

            while (sSalnum.Length > 7)
                sSalnum = sSalnum.Substring(1);
            this.sSalnum = sSalnum;
            this.sWerkgeverNr = sWerkgeverNr;
            this.sDatum = sDatum;
            this.sLooncode = sLooncode;

            try
            {

                this.iUren = (int)(Convert.ToInt32(sUren));

            }
            catch (Exception ex)
            {
                this.iUren = 0;
                Algemeen.Log.Verwerking("record niet correct: " + sSalnum + " | " + sDatum);
                Algemeen.Log.Exception(ex);
            }
            this.dtDatum = Algemeen.Proces.dtStringToDate(sDatum);
            //BEREKEN WEEK VH JAAR
            CultureInfo myCI = new CultureInfo("nl-BE");
            Calendar myCal = myCI.Calendar;

            // Gets the DTFI properties required by GetWeekOfYear.
            CalendarWeekRule myCWR = myCI.DateTimeFormat.CalendarWeekRule;
            DayOfWeek myFirstDOW = myCI.DateTimeFormat.FirstDayOfWeek;

            int week = myCal.GetWeekOfYear(dtDatum, myCWR, myFirstDOW);
            DayOfWeek laatsteDag = new DateTime(dtDatum.Year, 12, 31).DayOfWeek;
            if ((week == 1) && (laatsteDag != DayOfWeek.Sunday))
            {
                this.week = myCal.GetWeekOfYear(new DateTime(dtDatum.Year, 12, 31), myCWR, myFirstDOW);
            }
            else
            {
                this.week = week;
            }
           
        }

        public List<Record> omboekMeerUren()
        {
            List<Record> omgeboekt = new List<Record>();

            int aant1010 = this.iUren;
            foreach (Record record in Record.lrRecords)
            {
                if (record.sSalnum == this.sSalnum)
                {
                }
                if ((record.sSalnum == this.sSalnum) && (record.sLooncode == "7010") && (aant1010 > 0) && (this.week == record.week) && (record.dtDatum.DayOfWeek != DayOfWeek.Sunday))
                {
                    if (record.iUren == aant1010)
                    {
                        record.iUren = 0;
                        record.sLooncode = "0000";
                        Record nieuw = new Record(record.sSalnum, this.sWerkgeverNr, record.sDatum, this.sLooncode, aant1010.ToString(), true);
                        omgeboekt.Add(nieuw);
                        aant1010 = 0;
                    }
                    else if (record.iUren < aant1010)
                    {

                        aant1010 = aant1010 - record.iUren;
                        int aant1041 = record.iUren;
                        record.iUren = 0;
                        record.sLooncode = "0000";
                        Record nieuw = new Record(record.sSalnum, this.sWerkgeverNr, record.sDatum, this.sLooncode, aant1041.ToString(), true);
                        omgeboekt.Add(nieuw);

                    }
                    else
                    {
                        record.iUren = record.iUren - aant1010;

                        Record nieuw = new Record(record.sSalnum, this.sWerkgeverNr, record.sDatum, this.sLooncode, aant1010.ToString(),true);
                        omgeboekt.Add(nieuw);
                        aant1010 = 0;
                    }
                }

            }
            this.iUren = 0;
            this.sLooncode = "0000";
            return omgeboekt;
        }
        public void checkDubbel(List<Record> records)
        {
            foreach (Record record in records)
            {
                if ((this.sDatum == record.sDatum) &&  (this.sLooncode == record.sLooncode) && (this.sSalnum == record.sSalnum))
                {
                    string salnum = this.sSalnum;
                    this.sSalnum = "weg";
                    if (record.sSalnum == "weg")
                    {
                        this.sSalnum = salnum;
                    }
                    else
                    {
                        int uren = record.iUren;
                        int bijtel = this.iUren;
                        uren = uren + bijtel;
                        record.iUren = uren;
                    }
                }

            }
        }
        public Boolean bestaatRecordZelfdeDag()
        {
            foreach (Record record in Record.lrRecords)
            {
                string salnum = this.sSalnum;
                // CHECK OF THIS HETZELFDE RECORD IS ALS RECORD
                //zet salarisnummer van this op "weg"
                this.sSalnum = "weg";
                int checkZelfde = 1;
                //als het salarisnummer van record niet gelijk is aan "weg" gaat het om een ander record
                if (record.sSalnum != "weg")
                {
                    checkZelfde = 0;
                }
                //salnum terug op originele plaatsen
                this.sSalnum = salnum;
                if ((checkZelfde == 0) && (record.sSalnum == this.sSalnum) && (record.sDatum == this.sDatum))
                {
                    return true;
                }

            }
            return false;
        }
        public int CompareTo(object obj)
        {
            Record Compare = (Record)obj;
            string sortOn = this.sSalnum + this.sDatum;
            string sortOnObject = Compare.sSalnum + Compare.sDatum;
            int result = sortOn.CompareTo(sortOnObject);
            if (result == 0)
                result = sortOn.CompareTo(sortOnObject);
            return result;
        }
    }
}
