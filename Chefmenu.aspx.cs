﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace WebApplication8
{
    public partial class Chefmenu : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlDataSource1.SelectCommand = "SELECT MIN(aanvraagDetail.datum) AS 'beginSort', MAX(aanvraagDetail.datum) AS 'eindSort',aanvraag.id AS 'AANVRAAG', aanvraag.werknemerSalnum,(SELECT naam from werknemer where werknemer.salnum = werknemerSalnum) as werknemerNaam, aanvraag.ucatId, SUBSTRING(MIN(aanvraagDetail.datum), 7, 2) + '/' + SUBSTRING(MIN(aanvraagDetail.datum), 5, 2) + '/' + SUBSTRING(MIN(aanvraagDetail.datum), 1, 4) AS Begindag, SUBSTRING(MAX(aanvraagDetail.datum), 7, 2) + '/' + SUBSTRING(MAX(aanvraagDetail.datum), 5, 2) + '/' + SUBSTRING(MAX(aanvraagDetail.datum), 1, 4) AS Einddag, CASE WHEN status = 0 THEN 'IN AANVRAAG' WHEN status = 1 THEN 'ANNULATIE' WHEN status = 2 THEN 'AFKEURING' WHEN status= 3 THEN 'IN WACHT' WHEN status=4 THEN 'GOEDGEKEURD' END AS statusText, status, CASE WHEN aanvraagDetail.type = 1 THEN 'HELE DAG' WHEN aanvraagDetail.type = 2 THEN 'VOORMIDDAG' WHEN aanvraagDetail.type = 3 THEN 'NAMIDDAG' WHEN aanvraagDetail.type = 0 THEN 'VAN/TOT' END AS type, SUBSTRING(aanvraagDetail.van, 1, 2) + ':' + SUBSTRING(aanvraagDetail.van, 3, 2) AS 'van', SUBSTRING(aanvraagDetail.tot, 1, 2) + ':' + SUBSTRING(aanvraagDetail.tot, 3, 2) AS 'tot' FROM aanvraag INNER JOIN aanvraagDetail ON aanvraag.id = aanvraagDetail.aanvraagId INNER JOIN werknemer ON aanvraag.werknemerSalnum = werknemer.salnum WHERE werknemer.afd IN (SELECT afdKp FROM chef WHERE aanvraagDetail.datum<>'00000000' AND werknemerSalnum='" + Session["salnum"] + "') OR werknemer.kp=(SELECT MAX(afdKp) FROM chef WHERE werknemerSalnum='" + Session["salnum"] + "')  GROUP BY aanvraagDetail.aanvraagId, aanvraag.werknemerSalnum, aanvraag.ucatId, aanvraagDetail.status, aanvraagDetail.type, aanvraagDetail.van, aanvraagDetail.tot, aanvraag.id";
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        protected void btnAllesGoedkeuren_Click(object sender, EventArgs e)
        {
            SqlConnection conn = createConnection();
            string status = ddlStatus.SelectedValue;
            SqlCommand insert = new SqlCommand("UPDATE aanvraagDetail SET status = " + status + ", export=1 WHERE status=0  AND (SELECT kp FROM werknemer where salnum=(SELECT werknemerSalnum FROM aanvraag WHERE aanvraag.id = aanvraagId))=(SELECT kp FROM werknemer WHERE salnum='" + Session["salnum"] + "')", conn);
            insert.ExecuteScalar();
        }
        //  MAAK SQL CONNECTIE
        protected SqlConnection createConnection()
        {
            //  DEFINIEER CONNECTIONSTRING
            string connstring = @"Data Source=localhost\SQLEXPRESS;Initial Catalog=DB;Integrated Security=SSPI;";
            //  VERBIND
            SqlConnection conn = new SqlConnection(connstring);
            conn.Open();
            //RETURN DE CONNECTIE
            return conn;
        }
    }
}
